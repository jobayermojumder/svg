<?php
$directory = 'assets/artboard/';
$files = array_slice(scandir($directory), 2);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!-- jQuery library 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
	<script src="assets/js/jquery-3.3.1.min.js"></script>
    
	<script src="assets/js/jscolor.js"></script>
	<title>Document</title>
	<style type="text/css">
	tr{
		text-align: center;
	}
	img{
		width: 140px;
	}
	.art-class{
		width: 35%;
		float: right;
		height: 600px;
		overflow: scroll;
	}
	table, td, tr{
		border:1px solid;
	}
</style>
</head>
<body>
	<div style="width: 65%;float: left;">
		<select id="number-of-div">
			<option selected>Select number</option>
			<option value="1">make 1</option>
			<option value="2">make 2</option>
			<option value="3">make 3</option>
			<option value="4">make 4</option>
			<option value="5">make 5</option>
		</select>

		<select id="select-svg-id">
			<option value="" selected>Select SVG</option>
		</select>

		<input type="number" id="row" placeholder="row">
		<input type="number" id="column" placeholder="column">
		<button onclick="Show();">Show</button> &nbsp&nbsp&nbsp<button id = "save-button">Save</button>
		<input type="color" name="favcolor" value="" hidden id = "color-picker">

		<br><br>
		<svg width="800" height="300" id='main-svg'>
		</svg>
	</div>
	<div class="art-class">
		<div>
			<select id="artboard">
				<option value="">None</option>
				<?php 
				$i=1;
				foreach($files as $file){ ?>
				<option value="<?=$file?>" ><?=$i++?>. <?=$file?></option>
				<?php } ?>
			</select>
			<button onclick="clearSVG();">Clear SVG</button>
			<div style="width: 200px; height: 135px; float: right; text-align: center;">
				<img src="" id="selected-image-file" width="200px" height="130px">
			</div>
		</div>

		<div>
			<table>
				<tbody>
					<tr>
						<?php 
						$i=1;
						$j=1;
						$first=0;
						foreach($files as $file){ ?>
						<?php if ($i==4) { 
							$i=1;
							?>
						</tr>
						<tr>
							<td id="<?=$file?>" onclick="setArtBoard(this.id);"><?=$j?>. <img src="assets/artboard/<?=$file?>"></td>
							<?php } else { ?>
							<td id="<?=$file?>" onclick="setArtBoard(this.id);" ><?=$j?>. <img src="assets/artboard/<?=$file?>"></td>
							<?php } ?>
							<?php
							$i++;
							$j++;
						} ?>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<script>
		var currentElement = null;
		$(document).ready(function(){
			$( "#number-of-div" ).change(function() {
				var max = $(this).val();
				make_div(max);
			});

			$( "#save-button" ).click(function() {
				saveFile();
			});

			$('#color-picker').on('input', function() {
				currentElement.setAttribute("style", "fill: " + $("#color-picker").val());
			});
		});

		function make_div(max){
			var option = '<option value="" selected>Select SVG</option>';
			var maximum_width = 800;
			var maximum_height = 300;
			var main_svg = document.getElementById("main-svg");
			$("#main-svg").empty();
			var x = 0;
			var color = ['white','red','green','yellow','black','blue']
			for (var i =1; i <= max; i++) {
				var xmlns = "http://www.w3.org/2000/svg";
				var boxWidth = maximum_width/max;
				var boxHeight = maximum_height; 

				var svgElem = document.createElementNS (xmlns, "svg");
				svgElem.setAttributeNS (null, "width", boxWidth);
				svgElem.setAttributeNS (null, "height", boxHeight);
				svgElem.setAttributeNS (null, "x", x);
				svgElem.setAttributeNS (null,'id', "svg"+i);
				x = x + maximum_width/max;
				main_svg.appendChild(svgElem);

				var rect = document.createElementNS("http://www.w3.org/2000/svg","rect");
				rect.setAttribute("width",maximum_width/max);
				rect.setAttribute("height",maximum_height);
				rect.setAttribute("onclick", "chooseColor(this)");
				rect.style.fill = color[i];
				var child_svg = document.getElementById("svg"+i);
				child_svg.appendChild(rect);
				option += '<option value="svg'+i+'">SVG "'+color[i]+'" </option>' 
			}
			$('#select-svg-id').html(option);
		}

		function Show(){
			var row = $('#row').val();
			var column = $('#column').val();
			var svg_id = $('#select-svg-id').val();
			if (row && column && svg_id ){
				row_column(row, column, svg_id)
			}else{
				alert("Row or column or svg not valid.");
			}
		}

		function row_column(row, column, svg_id){
			var color = ["AliceBlue","AntiqueWhite","Aqua","Aquamarine","Azure","Beige","Bisque","Black","BlanchedAlmond","Blue","BlueViolet","Brown","BurlyWood","CadetBlue","Chartreuse","Chocolate","Coral","CornflowerBlue","Cornsilk","Crimson","Cyan","DarkBlue","DarkCyan","DarkGoldenRod","DarkGray","DarkGrey","DarkGreen","DarkKhaki","DarkMagenta","DarkOliveGreen","Darkorange","DarkOrchid","DarkRed","DarkSalmon","DarkSeaGreen","DarkSlateBlue","DarkSlateGray","DarkSlateGrey","DarkTurquoise","DarkViolet","DeepPink","DeepSkyBlue","DimGray","DimGrey","DodgerBlue","FireBrick","FloralWhite","ForestGreen","Fuchsia","Gainsboro","GhostWhite","Gold","GoldenRod","Gray","Grey","Green","GreenYellow","HoneyDew","HotPink","IndianRed","Indigo","Ivory","Khaki"];
			$("#"+svg_id).empty();
			var main_width = $("#"+svg_id).attr('width');
			var main_height = $("#"+svg_id).attr('height');
			width = main_width/column;
			height = main_height/row;
			var rand_color = 0;
			for (var i = 0; i < column; i++) {
				for (var j = 0; j < row; j++) {
					rand_color =  Math.floor((Math.random() * 10) + 1);
					var rect = document.createElementNS("http://www.w3.org/2000/svg","rect");
					rect.setAttribute("width",width);
					rect.setAttribute("height",height);
					rect.setAttribute("x",width*i);
					rect.setAttribute("y",height*j);
					rect.setAttribute("stroke-width",'2px');
					rect.setAttribute("id",svg_id+i+j);
					rect.style.stroke = "#000000";
					rect.style.fill = "#FFFFFF";
					rect.setAttribute("onclick", "setImage(this.id)");
					var child_svg = document.getElementById(svg_id);
					child_svg.appendChild(rect);
				}
			}
		}

		function saveFile() {
			var text = document.getElementById("main-svg").outerHTML;
			var filename = "image.svg"
			var element = document.createElement('a');
			element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
			element.setAttribute('download', filename);

			element.style.display = 'none';
			document.body.appendChild(element);

			element.click();
			document.body.removeChild(element);
		}

		function chooseColor(e) {
			currentElement = e;
			$("#color-picker").click();
		}

		function setImage(id){
			var width = $('#'+id).attr('width');
			var height = $('#'+id).attr('height');
			var x = $('#'+id).attr('x');
			var y = $('#'+id).attr('y');
			var imageFileName = $('#artboard').val();
			if (imageFileName) {

				var xmlns = "http://www.w3.org/2000/svg";
				var svgElem = document.createElementNS (xmlns, "svg");
				svgElem.setAttributeNS (null, "width", width);
				svgElem.setAttributeNS (null, "height", height);
				svgElem.setAttributeNS (null, "x", x);
				svgElem.setAttributeNS (null, "y", y);
				svgElem.setAttributeNS (null,'id', id);
				svgElem.setAttribute("onclick", "setSvgImage(this.id)");
				$( "rect#"+id).replaceWith(svgElem);
				var image = document.getElementById(id);

				xhr = new XMLHttpRequest();
				xhr.open("GET","assets/artboard/"+imageFileName,false);
				xhr.overrideMimeType("image/svg+xml");
				xhr.send("");
				image.appendChild(xhr.responseXML.documentElement);

			} else {
				alert('No art selected');
			}

			/*var pid = $("#"+id).parent().attr("id");
			var width = $("#"+id).parent().attr("width");
			$( "rect#"+id).replaceWith( "<h2>New heading</h2>" );
			console.log(width);*/
		}

		function setSvgImage(id){
			var imageFileName = $('#artboard').val();
			if (imageFileName) {
				$("#"+id).empty();

				var image = document.getElementById(id);
				xhr = new XMLHttpRequest();
				xhr.open("GET","assets/artboard/"+imageFileName,false);
				xhr.overrideMimeType("image/svg+xml");
				xhr.send("");
				image.appendChild(xhr.responseXML.documentElement);

			} else {
				alert('No art selected');
			}
		}

		function setArtBoard(id){
			$('#artboard').val(id);
			$('#selected-image-file').attr("src",'assets/artboard/'+id);
		}

		function clearSVG(){
			$('#selected-image-file').attr("src",'');
			$('#artboard').val('');
		}

	</script>
</body>
</html>
