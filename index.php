<?php
$directory = 'assets/artboard/';
$files = array_slice(scandir($directory), 2);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<script src="assets/js/jquery-3.3.1.min.js"></script>
	<script src="assets/js/download2.js"></script>

	<script src="assets/js/jscolor.js"></script>
	
	<title>Document</title>
	<style type="text/css">
	tr{
		text-align: center;
	}
	img{
		width: 110px;
	}
	.art-class{
		/* width: 35%;
		float: right; */
		/* height: 600px; */
		/* overflow: scroll; */
	}
	table, td, tr{
		border:1px solid;
	}
	.delete{
		color: red;
		display: none;
	}
	input[type="number"]{
		width: 60px;
	}
	.bottom-part{
		margin-top: 10px;
	}
	.active {
		border: 4px solid #F00 !important;
	}
	.margin-left {
		margin-left: 20px;
	}
	.download-button {
		position: absolute;
		right:30px;
		padding:7px;
		background-color:#1e90ff;
		color: white;
		cursor: pointer;
	}
	.download-input {
		position: absolute;
		right: 150px;
		padding:7px;
	}
</style>
</head>
<body>
	<div style="width: 100%;">

		<div>
			<span><b>Jomin Section</b></span>
			<select id="number-of-div">
				<option value="1">1 Section</option>
				<option value="2">2 Sections</option>
				<option value="3" selected>3 Sections</option>
				<option value="4">4 Sections</option>
				<option value="5">5 Sections</option>
			</select>

			<span class="margin-left"><b>Upper Paar Section</b></span>
			<select id="upper-paar-div">
				<option value="1">1 Section</option>
				<option value="2">2 Sections</option>
				<option value="3" selected>3 Sections</option>
				<option value="4">4 Sections</option>
				<option value="5">5 Sections</option>
			</select>

			<span class="margin-left"><b>Lower Paar Section</b></span>
			<select id="lower-paar-div">
				<option value="1">1 Section</option>
				<option value="2">2 Sections</option>
				<option value="3" selected>3 Sections</option>
				<option value="4">4 Sections</option>
				<option value="5">5 Sections</option>
			</select>
			<input type="text" id="input-save-image" class="download-input">
			<button id = "save-button" class="download-button">Save Design</button>
			<input type="color" name="favcolor" value="" hidden id = "color-picker">
			<br><br>
		</div>
		<div>
			<span><b>Upper Paar Height</b> </span><input type="number" id="paar-height-upper" placeholder="height">

			<span class="margin-left"><b>Lower Paar Height</b> </span><input type="number" id="paar-height-lower" placeholder="height">

			<!-- <span>(size in PX. Total Saree height 400px.)</span> -->
		</div>
		<br>	
		<div>
			<b>Jomin</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<select id="select-svg-id">
				<option value="" selected>Select Section</option>
			</select>&nbsp&nbsp&nbsp
			<input type="number" id="row" value = 2 placeholder="row">&nbsp&nbsp&nbsp
			<input type="number" id="column" value = 2 placeholder="column">&nbsp&nbsp&nbsp
			<button onclick="makeTable();">Splite Section</button>&nbsp&nbsp&nbsp
			<button onclick="clearSVG();">Clear Section</button>&nbsp&nbsp&nbsp
			<button onclick="placeAll();">Put Shape in Selected Section</button>
			<p><b>Upper Paar</b>&nbsp;&nbsp;&nbsp;
				<select id="select-svg-paar-upper">
					<option value="" selected>Select Section</option>
				</select>&nbsp&nbsp&nbsp
				<input type="number" id="upper-row" placeholder="row">&nbsp&nbsp&nbsp
				<input type="number" id="upper-column" placeholder="column">&nbsp&nbsp&nbsp
				<button onclick="makePaarTable('paar-upper');">Splite Section</button>&nbsp&nbsp&nbsp
				<button onclick="paarClearSVG('paar-upper');">Clear Section</button>&nbsp&nbsp&nbsp
				<button onclick="paarPlaceAll('paar-upper');">Put Shape in Selected Section</button>
				<p><b>Lower Paar</b>&nbsp;&nbsp;&nbsp;
					<select id="select-svg-paar-lower">
						<option value="" selected>Select Section</option>
					</select>&nbsp&nbsp&nbsp
					<input type="number" id="lower-row" placeholder="row">&nbsp&nbsp&nbsp
					<input type="number" id="lower-column" placeholder="column">&nbsp&nbsp&nbsp
					<button onclick="makePaarTable('paar-lower');">Splite Section</button>&nbsp&nbsp&nbsp
					<button onclick="paarClearSVG('paar-lower');">Clear Section</button>&nbsp&nbsp&nbsp
					<button onclick="paarPlaceAll('paar-lower');">Put Shape in  Selected Section</button>
				</div>


				<div>
					<svg id = "full-svg"  viewBox="0 0 1100 450">
						<svg width="1100" height="30" id='paar-upper' y=0 ></svg>
						<svg width="1100" height="360" id='main-svg' y = 31></svg>
						<svg width="1100" height="30" id='paar-lower' y = 392></svg>
					</div>
				</div>

				<div class="bottom-part">
					<div class="art-class">
						<div style = "float: left; margin-right: 200px; margin-bottom: 40px">
							<select id="artboard" style="display: none;">
								<option value="">None</option>
								<?php 
								$i=1;
								foreach($files as $file){ ?>
								<option value="<?=$file?>" ><?=$i++?>. <?=$file?></option>
								<?php } ?>
							</select>
							<div style="width: 250px; text-align: center;" draggable="true" ondragstart="drag(event)">
								<svg  id='display-svg-image' style="width: 200px; height: 135px;"></svg>
								<button onclick="clearSVGImage();">Clear Art Board</button>
								<button onclick="rotateDisplaySvg()">Rotate Design</button>
							</div>	
							<p style="text-align: center;">Art Board</p>					
						</div>

						<div>
							<table>
								<tbody>
									<tr>
										<?php 
										$i=1;
										$j=1;
										$first=0;
										foreach($files as $file){ ?>
										<?php if ($i==7) { 
											$i=1;
											?>
										</tr>
										<tr>
											<td id="<?=$file?>" onclick="setArtBoard(this.id);"><?=$j?>. <img src="assets/artboard/<?=$file?>"></td>
											<?php } else { ?>
											<td id="<?=$file?>" onclick="setArtBoard(this.id);" ><?=$j?>. <img src="assets/artboard/<?=$file?>"></td>
											<?php } ?>
											<?php
											$i++;
											$j++;
										} ?>
									</tr>
								</tbody>
							</table>
						</div>
						<p style="text-align: center;">Preset Shapes</p>
					</div>
				</div>
				<script>
					function allowDrop(ev) {
						ev.preventDefault();
					}

					function drag(ev) {

						ev.dataTransfer.setData("text/html", ev.target.id);
					}

					function drop(ev) {
						ev.preventDefault();
						console.log(ev.target.id);
						$(ev.target).trigger('click')
					}


					var currentElement = null;
					var current_div = 3;
					var main_height = 420;
					var main_width = 100;
					var paar_height_upper = 30;
					var paar_height_lower = 30;
					var jomin_height = 360;
					var rotate=0;

					$(document).ready(function(){


						$('#paar-height-upper').val('30');
						$('#paar-height-lower').val('30');

						var div_num = '3';
						make_paar_div(div_num, 'paar-upper');
						make_paar_div(div_num, 'paar-lower');

						make_jomin_div(current_div);

						$( "#number-of-div" ).change(function() {
							max = $(this).val();
							current_div = max;
							make_jomin_div(max);
						});

						$( "#upper-paar-div" ).change(function() {
							var upper_paar_div = $(this).val();
							make_paar_div(upper_paar_div, 'paar-upper');
						});

						$( "#lower-paar-div" ).change(function() {
							var lower_paar_div = $(this).val();
							make_paar_div(lower_paar_div, 'paar-lower');
						});

						$( "#save-button" ).click(function() {
							saveFile();
						});

						$('#color-picker').on('input', function() {
							currentElement.setAttribute("style", "fill: " + $("#color-picker").val());
						});

						$("#paar-height-upper").change(function(){
							resize_saree();
						});
						$("#paar-height-lower").change(function(){
							resize_saree();
						});

					});


		//----------------resize saree with paar height width-----------//
		function resize_saree(){
			paar_height_upper = parseInt($('#paar-height-upper').val());
			paar_height_lower = parseInt($('#paar-height-lower').val());

			if (paar_height_upper>=0  && paar_height_lower>=0 ) {
				jomin_height = main_height-(paar_height_upper + paar_height_lower);
				/*alert(jomin_height);*/
				$("#main-svg").attr('y', paar_height_upper+1);
				$("#main-svg").attr('height', jomin_height);
				
				$("#paar-upper").attr('height', paar_height_upper);
				
				$("#paar-lower").attr('height', paar_height_lower);
				$("#paar-lower").attr('y', paar_height_upper+jomin_height+2);
			}else{
				console.log("large value.");
			}
		}

		//----------create svg main div------------//
		function make_jomin_div(max){
			var option = '<option value="" selected>Select Section</option>';
			var maximum_height = jomin_height;
			var main_svg = document.getElementById("main-svg");
			$("#main-svg").empty();
			var x = 0;
			var color = ['white','red','green','yellow','black','blue']
			for (var i =1; i <= max; i++) {
				var xmlns = "http://www.w3.org/2000/svg";
				var boxWidth = 100/max;
				var boxHeight = maximum_height; 

				var svgElem = document.createElementNS (xmlns, "svg");
				svgElem.setAttributeNS (null, "width", boxWidth+'%');
				svgElem.setAttributeNS (null, "height", '100%');
				svgElem.setAttributeNS (null, "x", x+'%');
				svgElem.setAttributeNS (null,'id', "svg"+i);
				x = x + main_width/max;
				main_svg.appendChild(svgElem);

				var rect = document.createElementNS("http://www.w3.org/2000/svg","rect");
				rect.setAttribute("width",'100%');
				rect.setAttribute("height",'100%');
				rect.setAttribute("onclick", "chooseColor(this)");
				rect.style.fill = color[i];
				var child_svg = document.getElementById("svg"+i);
				child_svg.appendChild(rect);

				// option += '<option value="svg'+i+'">SVG "'+color[i]+'" </option>' 
				option += '<option value="svg'+i+'">'+color[i]+" "+'Section </option>' 
			}
			$('#select-svg-id').html(option);
		}

		//-------------make table with row column----------//
		
		function makeTable(){
			var row = $('#row').val();
			var column = $('#column').val();
			var svg_id = $('#select-svg-id').val();
			if (row && column && svg_id ){
				row_column(row, column, svg_id)
			}else{
				alert("Please select a section and write row and column number");
			}
		}

		function row_column(row, column, svg_id){
			var color = ["AliceBlue","AntiqueWhite","Beige","Bisque","Black","BlanchedAlmond","Blue","BlueViolet","Brown","BurlyWood","CadetBlue","Chartreuse","Chocolate","Coral","CornflowerBlue","Cornsilk","Crimson","Cyan","DarkBlue","DarkGrey","DarkGreen","DarkKhaki","DarkMagenta","DarkSlateGray","DarkSlateGrey","DarkTurquoise","DarkViolet","DeepPink","Green","GreenYellow","HoneyDew","HotPink","IndianRed","Indigo","Ivory","Khaki"];
			
			$("#"+svg_id).empty();

			var main_width = parseInt($("#"+svg_id).attr('width'));
			var main_height = parseInt($("#"+svg_id).attr('height'));
			var child_svg = document.getElementById(svg_id);

			child_svg.setAttribute("row",row);
			child_svg.setAttribute("column",column);

			var rect = document.createElementNS("http://www.w3.org/2000/svg","rect");
			rect.setAttribute("width", '100%');
			rect.setAttribute("height", '100%');
			rect.setAttribute("fill", 'white');
			rect.setAttribute("onclick", 'chooseColor(this)');
			child_svg.appendChild(rect);


			width = 100/column;
			height = 100/row;
			var rand_color = 0;
			for (var i = 0; i < column; i++) {
				for (var j = 0; j < row; j++) {
					rand_color =  Math.floor((Math.random() * 10) + 1);
					var rect = document.createElementNS("http://www.w3.org/2000/svg","rect");
					rect.setAttribute("width",width+'%');
					rect.setAttribute("height",height+'%');
					rect.setAttribute("x",width*i+'%');
					rect.setAttribute("y",height*j+'%');
					rect.setAttribute("id",svg_id+i+j);
					rect.setAttribute("stroke-width",'2px');
					rect.style.stroke = "#000000";
					rect.style.fill = "#FFFFFF";
					rect.setAttribute("onclick", "setImage(this.id)");
					rect.setAttribute("ondrop", "drop(event)");
					rect.setAttribute("ondragover", "allowDrop(event)");
					
					child_svg.appendChild(rect);
				}
			}
		}

		//------------------save as svg image-------------//

		
		function saveFile() {
			var filename = $('#input-save-image').val();
			if (filename) {
				filename = filename + '.svg';
			} else {
				filename = 'design.svg';
			}
			var text = document.getElementById("full-svg").outerHTML;
			download(new Blob([text]), filename, "text/plain");
			//$('#input-save-image').val('');
		}




		//-------choose color onclick-----------//
		function chooseColor(e) {
			currentElement = e;
			$("#color-picker").click();
		}

		//-------------set image in every single row------------//
		function setImage(id){
			var width = $('#'+id).attr('width');
			var height = $('#'+id).attr('height');
			var x = $('#'+id).attr('x');
			var y = $('#'+id).attr('y');
			var imageFileName = $('#artboard').val();
			if (imageFileName) {
				$("#"+id).empty();
				var xmlns = "http://www.w3.org/2000/svg";
				var svgElem = document.createElementNS (xmlns, "svg");
				svgElem.setAttributeNS (null, "width", width);
				svgElem.setAttributeNS (null, "height", height);
				svgElem.setAttributeNS (null, "x", x);
				svgElem.setAttributeNS (null, "y", y);
				svgElem.setAttributeNS (null,'id', id);
				svgElem.setAttribute("onclick", "setSvgImage(this.id)");
				svgElem.setAttribute("ondrop", "drop(event)");
				svgElem.setAttribute("ondragover", "allowDrop(event)");

				$( "rect#"+id).replaceWith(svgElem);
				var image = document.getElementById(id);
				var html = $("#display-svg-image").html();

			// var elem = $("#display-svg-image").first().find("svg");
			// var inner = '<g style="transform: rotate(97deg);">' + elem.html() + '</g>';
			// var inner = '<g transform="rotate(90 297.64 420.945)">' + elem.html() + '</g>';
			// var inner = elem.html();
			// elem.empty();
			// elem.append(inner);
			// $('#display-svg-image').html(elem[0].outerHTML);
			// var html = elem[0].outerHTML;

			$('#'+id).html(html);

		} else {
			alert('No art selected');
		}
	}


	function setSvgImage(id){
		var imageFileName = $('#artboard').val();
		if (imageFileName) {
			$("#"+id).empty();
			var html = $("#display-svg-image").html();
			$("#"+id).html(html);

		} else {
			alert('No art selected');
		}
	}

	function setArtBoard(id){
		$('#artboard').val(id);
		var imageFileName = $('#artboard').val();
		if (imageFileName) {
			$("#display-svg-image").empty();
			var image = document.getElementById('display-svg-image');
			xhr = new XMLHttpRequest();
			xhr.open("GET","assets/artboard/"+imageFileName,false);
			xhr.overrideMimeType("image/svg+xml");
			xhr.send("");
			image.appendChild(xhr.responseXML.documentElement);

		}
			//$('#selected-image-file').attr("src",'assets/artboard/'+id);
		}

		function clearSVGImage(){
			$("#display-svg-image").empty();
			$('#display-svg-image').css('transform', 'rotate(0deg)');
			$('#artboard').val('');
		}

		function clearSVG(){
			var svg_id = $('#select-svg-id').val();
			if (svg_id) {
				var child_svg = document.getElementById(svg_id);
				$("#"+svg_id).empty();
				var rect = document.createElementNS("http://www.w3.org/2000/svg","rect");
				rect.setAttribute("width",'100%');
				rect.setAttribute("height",'100%');
				rect.setAttribute("onclick", "chooseColor(this)");
				rect.setAttribute("stroke-width",'2px');
				rect.style.stroke = "#000000";
				rect.style.fill = "#FFFFFF";
				child_svg.appendChild(rect);
			} else {
				alert('No area selected.');
			}

		}

		function placeAll(){
			var imageFileName = $('#artboard').val();

			if (imageFileName) {
				var main_svg_id = $('#select-svg-id').val();
				var main_svg = document.getElementById(main_svg_id);
				var row = $('#'+main_svg_id).attr('row');
				var column = $('#'+main_svg_id).attr('column');
				for (var i = 0; i < column; i++) {
					for (var j = 0; j < row; j++) {
						setImage(main_svg_id+i+j);
						//console.log(main_svg_id+i+j);
					}
				}
			}else{
				console.log('Artboard is empty.');
			}			
		}

		function rotateDisplaySvg(){
			rotate+=10;
			if (rotate>=360) {
				rotate = rotate-360;
			}

			
			var viewbox = $('#display-svg-image > svg:first').attr('viewBox').split(" ").map(Number);
			var temp = 'rotate(' + rotate + ' ' + (viewbox[2]/2) +  ' ' + (viewbox[3]/2) + ')';
			
			var element = $('#display-svg-image > svg:first > g:first');
			$(element).attr('transform', temp);

		}
		

		//--------------------paar section------------------//

		//--------create paar div ------------//
		function make_paar_div(max, id){
			var option = '<option value="" selected>Select Section</option>';
			if (id=='paar-upper') {
				var height = paar_height_upper;
			} else if(id=='paar-lower') {
				var height = paar_height_lower;
			}

			var paar_svg = document.getElementById(id);
			$("#"+id).empty();

			var rect = document.createElementNS("http://www.w3.org/2000/svg","rect");
			rect.setAttribute("width", '100%');
			rect.setAttribute("height", '100%');
			rect.setAttribute("fill", 'white');
			rect.setAttribute("onclick", 'chooseColor(this)');
			paar_svg.appendChild(rect);

			

			var x = 0;
			var color = ['white','red','green','yellow','black','blue']
			for (var i =1; i <= max; i++) {
				var xmlns = "http://www.w3.org/2000/svg";
				var boxWidth = 100/max;
				var boxHeight = height; 

				var svgElem = document.createElementNS (xmlns, "svg");
				svgElem.setAttributeNS (null, "width", boxWidth+'%');
				svgElem.setAttributeNS (null, "height", '100%');
				svgElem.setAttributeNS (null, "x", x+'%');
				svgElem.setAttributeNS (null,'id', id+"-svg-"+i);
				x = x + main_width/max;
				
				paar_svg.appendChild(svgElem);

				var rect = document.createElementNS("http://www.w3.org/2000/svg","rect");
				rect.setAttribute("width",'100%');
				rect.setAttribute("height",'100%');
				rect.style.fill = color[i];
				var child_svg = document.getElementById(id+"-svg-"+i);
				child_svg.appendChild(rect);
				option += '<option value="'+id+'-svg-'+i+'">'+color[i]+" "+'Section </option>'
			}
			$('#select-svg-'+id).html(option);
		}

		//--------------paar row column------------//
		function paar_row_column(row, column, svg_id){
			var color = ["AliceBlue","AntiqueWhite","Beige","Bisque","Black","BlanchedAlmond","Blue","BlueViolet","Brown","BurlyWood","CadetBlue","Chartreuse","Chocolate","Coral","CornflowerBlue","Cornsilk","Crimson","Cyan","DarkBlue","DarkGrey","DarkGreen","DarkKhaki","DarkMagenta","DarkSlateGray","DarkSlateGrey","DarkTurquoise","DarkViolet","DeepPink","Green","GreenYellow","HoneyDew","HotPink","IndianRed","Indigo","Ivory","Khaki"];
			
			$("#"+svg_id).empty();

			var main_width = parseInt($("#"+svg_id).attr('width'));
			var main_height = parseInt($("#"+svg_id).attr('height'));
			var child_svg = document.getElementById(svg_id);

			child_svg.setAttribute("row",row);
			child_svg.setAttribute("column",column);

			width = 100/column;
			height = 100/row;
			var rand_color = 0;
			for (var i = 0; i < column; i++) {
				for (var j = 0; j < row; j++) {
					rand_color =  Math.floor((Math.random() * 10) + 1);
					var rect = document.createElementNS("http://www.w3.org/2000/svg","rect");
					rect.setAttribute("width",width+'%');
					rect.setAttribute("height",height+'%');
					rect.setAttribute("x",width*i+'%');
					rect.setAttribute("y",height*j+'%');
					rect.setAttribute("id",svg_id+i+j);
					rect.setAttribute("stroke-width",'2px');
					rect.style.stroke = "#000000";
					rect.style.fill = "#FFFFFF";
					rect.setAttribute("onclick", "setImage(this.id)");
					rect.setAttribute("ondrop", "drop(event)");
					rect.setAttribute("ondragover", "allowDrop(event)");					
					child_svg.appendChild(rect);
				}
			}
		}

		function makePaarTable(id){
			var e = '';
			if (id == 'paar-upper') {
				e = 'upper';
			} else {
				e = 'lower';
			}
			var row = $('#'+e+'-row').val();
			var column = $('#'+e+'-column').val();
			var svg_id = $('#select-svg-'+id).val();
			if (row && column && svg_id ){
				paar_row_column(row, column, svg_id)
			}else{
				alert("Please select a section and write row and column number");
			}
		}

		function paarClearSVG(id){
			var svg_id = $('#select-svg-'+id).val();
			if (svg_id) {
				var child_svg = document.getElementById(svg_id);
				$("#"+svg_id).empty();
				var rect = document.createElementNS("http://www.w3.org/2000/svg","rect");
				rect.setAttribute("width",'100%');
				rect.setAttribute("height",'100%');
				rect.setAttribute("onclick", "chooseColor(this)");
				rect.setAttribute("stroke-width",'2px');
				rect.style.stroke = "#000000";
				rect.style.fill = "#FFFFFF";
				child_svg.appendChild(rect);
			} else {
				alert('No area selected.');
			}

		}

		function paarPlaceAll(id){
			var imageFileName = $('#artboard').val();
			var e = '';
			if (id == 'paar-upper') {
				e = 'upper';
			} else {
				e = 'lower';
			}
			if (imageFileName) {
				var svg_id = $('#select-svg-'+id).val();
				var main_svg = document.getElementById(svg_id);
				var row = $('#'+svg_id).attr('row');
				var column = $('#'+svg_id).attr('column');
				for (var i = 0; i < column; i++) {
					for (var j = 0; j < row; j++) {
						setImage(svg_id+i+j);
						//console.log(main_svg_id+i+j);
					}
				}
			}else{
				console.log('Artboard is empty.');
			}			
		}

		
	</script>
</body>
</html>
